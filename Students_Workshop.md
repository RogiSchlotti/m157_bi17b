# Module 126 Werkstattauftrag:			Network Time Protocol (NTP)

1. `$sudo apt-get update`   <br>Update lest euch alle in der [/etc/apt/sources.list](https://wiki.ubuntuusers.de/sources.list/) und in **/etc/apt/sources.list.d/** eingetragenen Paketquellen neu ein. Hierbei erfolgt eine Prüfung auf die Signatur der Paketlisten.`update` benötigt keine Angabe von Paketnamen.

2. `$apt-get -y install ntp` <br>Mit diesem Befehl installieren wir nun den NTP Dienst.<br><br>Was bewirkt die eingabe `-y`?<br>Notieren Sie Ihre Lösung in Ihrer Dokumentation.

3. Öffen Sie das "ntp configuration file" im Terminal .<br>Notieren Sie Ihre Befehlseingabe in Ihrer Dokumentation.<br>Tipp Nutze einen Texteditor .<br>

   Wie viele Poolzonen sind in der Datei standardmässig eingetragen? Vervollständigen Sie Ihren Lösungsweg mit einem Screenshot.

   Standard Poolzonen erkennen Sie an Ihrem installierten Linux, genauer gesagt, worauf ihre Software basiert. Bei einem Raspberry Pi wird normalerweise Raspbian basierend auf Debian installiert. 

   ![raspbian_download](images/raspbian_download.png)

   Am Schluss der Poolzone sorgt das Keyword **iburst** für eine schnelle Erstsynchronisierung beim Programmstart.  Sehr alte NTP Versionen unterstützen das Keyword **iburst** nicht. 



4. ` $ntpq -pn  `  <br>Dieser Befehl zeigt eine Liste der vorhanden Poolzonen an sowie weitere Informationen. <br>![ntpq_pn](images/ntpq_pn.png)

   Definition der Spalten <br>**remote**: Dort sehen Sie die Zeitserver aus der ntp.conf Datei.<br>`*` = Aktuelle Zeitquelle<br>`#`= source selected, distance exceeds maximum value (Distanz zu weit)<br>`o` = source selected, Pulse Per Second (PPS) used<br>`+` = source selected, included in final set (Für Mittelwertberechnung
   		einbezogen)<br>`x`= source false ticker (kein verlässlicher Wert)<br>`.` = source selected from end of candidate list<br>`–` = source discarded by cluster algorithm (zu grosse Abweichung)<br>`blank` = source discarded high stratum, failed sanity  



5. Jetzt müssen nochmals diese Einstellungen in der ntp.conf Datei vorgenommen werden.

   <br>`disable auth` / Das "#" entfernen und somit aktivieren<br>`broadcastclient` / Das "#" entfernen und somit aktivieren<br><br>`$/etc/init.d/ntp restart` Mit dem Befehl den ntp server neustarten.

6. Nun auf dem eigenen Notebook in die Windows 10 Einstellungen gehen unter "Datum und Uhrzeit" die Automatische Uhrzeit und Zeitzonen ausschalten, zusätzlich Datum und Uhrzeit in eine falsche Zeitangabe ändern und speichern.<br>![urhzeit](images/Inkeddatum_uhrzeit_LI.jpg)




7. Jetzt zu Verwandte Einstellungen navigieren /Internetzeit/Einstellungen ändern<br> ![verwante_einstellungen](images/verwante_einstellungen.png)

![internetzeit](images/internetzeit.png)



8. Hier ersetzen wir nun den Standart-Zeitserver mit der IP von unserem Raspberry Pi und Testen unser Raspberry Pi Zeitserver mit "Jetzt aktualisieren" <br>![internetzeiteinstellungen](images/internetzeiteinstellungen.png)

9. Wurde alles richtig konfiguriert, erscheint folgende Benachrichtigung und Sie haben den Workshop erfolgreich beendet. <br>![internetzeiteinstellungen_Test](images/internetzeiteinstellungen_Test.png)

<br>