# README für Module 126 Werkstattauftrag: Network Time Protocol (NTP)
### Vorbereitung und Einstieg:<br><br>

**Network Time Protocol (NTP)**<br>NTP ist das Standard Protokoll für die Synchronisierung von Uhren in Computersystemen über paketbasierte Kommunikationsnetz. UDP ist das verbindungslose Transportprotokoll das von NTP verwendet wird. Es wurde speziell entwickelt, um eine zuverlässige Zeitangabe über Netzwerke mit variabler Paketlaufzeit zu ermöglichen.<br><br>![Architecture_NTP_labels_de](images/Architecture_NTP_labels_de.png)<br> NTP ist in UNIX-artigen Betriebssystemen in Form des Hintergrundprozesses (daemon) ntpd implementiert, der sowohl das lokale System justieren als auch als Server die Zeit für andere Systeme bereitstellen kann. Windows-Systeme können ebenfalls ohne Zusatzsoftware die genaue Zeit mittels NTP aus dem Internet beziehen (Systemsteuerung "Datum und Uhrzeit" / "Internetzeit") und nach Bearbeitung eines Eintrags in der Registrierungsdatenbank auch über NTP bereitstellen.<br><br>**Und nun werden Sie in dem Werkstattauftrag einen eigenen Zeit Server erstellen:** 

Dokumentieren Sie in Stichworten/Screenshots wie Sie vorgehen, damit die Lehrperson Ihre Schritte entsprechend nachvollziehen kann. <br>(Nützlich für Punktevergabe) 

**Raspberry Pi Information:**<br>Raspian_Nettime_NTP.img bei Lehrperson beziehen.<br>Statische IP-Adresse: `172.16.17.95`<br>Raspberry Pi mit Klassenzimmer Netzwerk Per Lan Verbinden.<br>Username: `pi`<br>Password: `Nettime`<br>Verbindung über: empfohlen VNC Viewer<br>https://www.realvnc.com/de/connect/download/viewer/Win32 <br>Disk Imager Download:<br>https://sourceforge.net/projects/win32diskimager/files/latest/download?source=navbar<br><img src="images/win32_disk_imager.png" alt="win32diskimager" style="zoom: 67%;" /><br>Sie können auch einen anderen Disk Imager verwenden.<br><br>Nützliche Links und Tipps:<br>	-Vergiss die Berechtigungen nicht `sudo`!<br>	-Achtet auf das gleiche Netzwerk! (Host, Raspberry Pi) <br>https://www.pool.ntp.org/zone/ch

https://www.raspberrypi.org/downloads/raspbian/

https://www.meinberg.de/german/info/ntp.htm

https://wiki.ubuntuusers.de/apt/apt-get/#apt-get-update

https://scottontechnology.com

https://de.wikipedia.org/wiki/Network_Time_Protocol

https://wiki.ubuntuusers.de/sudo/